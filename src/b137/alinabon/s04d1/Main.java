package b137.alinabon.s04d1;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Car firstCar = new Car();
        firstCar.setBrand("Ertiga");
        firstCar.setName("Suzuki");
        firstCar.setYearOfMake(2000);
        firstCar.drive();

        Car secondCar = new Car("Avanza", "Toyota", 2005);
        secondCar.drive();

        // Scanner appScanner = new Scanner(System.in);
        // System.out.println("Input Car: ");
        // String name = appScanner.nextLine();

        // firstCar.setName(name);
        // firstCar.drive();
    }
}
