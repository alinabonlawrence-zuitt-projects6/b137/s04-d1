package b137.alinabon.s04d1;

public class Car {
    // Properties
    private String name;
    private String brand;
    private  int yearOfMake;

    // Constructors
    // 2 types of Constructors

    // 1. Empty Constructor
    public Car(){}

    // 2. Parameterized Constructor
    public Car(String name, String brand, int yearOfMake){
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
    }

    // Getters
    public String getName(){
        return  name;
    }

    public String getBrand(){
        return brand;
    }

    public int getYearOfMake(){
        return yearOfMake;
    }

    // Setters

    public void setName(String name) {
        this.name = name;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setYearOfMake(int yearOfMake) {
        this.yearOfMake = yearOfMake;
    }

    // Methods
    public void drive(){
        System.out.println("Driving the " + this.brand + " " + this.name + " " + this.yearOfMake);
    }


}
